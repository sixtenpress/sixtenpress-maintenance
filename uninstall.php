<?php
// make sure uninstallation is triggered
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit();
}

sixtenpressmaintenance_delete_plugin();
/**
 * uninstall - clean up database removing plugin options
 *
 * @since 1.0
 */
function sixtenpressmaintenance_delete_plugin() {
	delete_option( 'sixtenpressmaintenance' );

	//delete users
	sixtenpressmaintenance_maybe_delete_users();

	// remove capabilities
	sixtenpressmaintenance_remove_capabilities();
}

/**
 * remove capabilities
 *
 * @since 2.1
 */
function sixtenpressmaintenance_remove_capabilities() {
	global $wp_roles;
	foreach ( $wp_roles->get_names() as $role => $info ) {
		$get_role = get_role( $role );
		$get_role->remove_cap( 'sixtenpressmaintenance_view_site' );
	}
}

function sixtenpressmaintenance_maybe_delete_users() {
	$old_users = wp_get_users_with_no_role();
	if ( ! $old_users ) {
		return;
	}
	foreach ( $old_users as $user ) {
		$data = get_userdata( $user );
		if ( false !== strpos( $data->user_login, 'sixtenpressmaintenance' ) ) {
			wp_delete_user( $user );
		}
	}
}
