<?php

/**
 * Class SixTenPressMaintenanceCSS
 * @package SixTenPressMaintenance
 * @copyright 2017 Robin Cornett
 */
class SixTenPressMaintenanceCSS {

	protected $setting;

	/**
	 * Compile and return the minified styles.
	 * @return string
	 */
	public function get_style() {
		$this->setting = sixtenpressmaintenance_get_setting();
		$styles        = $this->get_styles();
		$css           = $this->get_body_styles( $styles );
		$css          .= $this->get_typography_styles( $styles );
		$css          .= $this->get_link_styles( $styles );
		$css          .= $this->get_button_styles( $styles );
		if ( false !== strpos( $this->setting['content'], 'img' ) ) {
			$css .= $this->get_image_styles();
		}
		if ( false !== strpos( $this->setting['content'], 'gallery' ) ) {
			$css .= $this->get_gallery_styles();
		}

		return apply_filters( 'sixtenpressmaintenance_style', $css, $this->setting );
	}

	/**
	 * Returns generally modified style elements as an array for use in CSS.
	 * @return array
	 */
	protected function get_styles() {
		$text_direction = function_exists( 'is_rtl' ) && is_rtl() ? 'rtl' : 'ltr';
		$font_family    = 'rtl' === $text_direction ? 'Tahoma, Arial' : '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif';

		return apply_filters( 'sixtenpressmaintenance_css_elements', array(
			'background'         => $this->setting['background'],
			'text'               => $this->setting['text'],
			'link'               => $this->setting['link'],
			'link_hover'         => $this->adjust_brightness( $this->setting['link'], 50 ),
			'font_family'        => $font_family,
			'content_background' => '#fff',
			'text_mid'           => $this->adjust_brightness( $this->setting['text'], 170 ),
		) );
	}

	/**
	 * Define the general body styles.
	 *
	 * @param $styles
	 *
	 * @return string
	 */
	protected function get_body_styles( $styles ) {
		return apply_filters( 'sixtenpressmaintenance_css_body', sprintf( '
			* { box-sizing: border-box; }
			body {
				background: %1$s;
				color: %2$s;
				font-family: %3$s;
				font-size: 1em;
				margin-top: 50px;
			}
			.content {
				background: %4$s;
				margin: 2em auto;
				padding: 2em;
				max-width: 700px;
				-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.13);
				box-shadow: 0 1px 3px rgba(0, 0, 0, 0.13);
				overflow: auto;
			}',
			$styles['background'],
			$styles['text'],
			$styles['font_family'],
			$styles['content_background']
		), $styles, $this->setting );
	}

	/**
	 * Define the typography styles.
	 *
	 * @param $styles
	 *
	 * @return string
	 */
	protected function get_typography_styles( $styles ) {
		return apply_filters( 'sixtenpressmaintenance_css_typography', sprintf( '
			h1 {
				border-bottom: 1px solid %1$s;
				clear: both;
				font-size: 2em;
				margin: 1em 0 0 0;
				padding: 0;
				padding-bottom: 7px;
			}

			p {
				line-height: 1.5;
				margin: 25px 0 20px;
			}

			code { font-family: Consolas, Monaco, monospace; }

			ul li { margin-bottom: 10px; }',
			$styles['text_mid']
		), $styles, $this->setting );
	}

	/**
	 * Define the link styles.
	 *
	 * @param $styles
	 *
	 * @return string
	 */
	protected function get_link_styles( $styles ) {
		return apply_filters( 'sixtenpressmaintenance_css_links', sprintf( '
			a { color: %1$s; }

			a:hover,
			a:active { color: %2$s; }

			a:focus {
				-webkit-box-shadow: 0 0 0 1px %2$s,
				0 0 2px 1px %1$s;
				box-shadow: 0 0 0 1px %2$s,
				0 0 2px 1px %1$s;
				outline: none;
			}',
			$styles['link'],
			$styles['link_hover']
		), $styles, $this->setting );
	}

	/**
	 * Define the button styles.
	 *
	 * @param $styles
	 *
	 * @return string
	 */
	protected function get_button_styles( $styles ) {
		return apply_filters( 'sixtenpressmaintenance_css_buttons', sprintf( '
			.button {
				background: %1$s;
				border: 1px solid %3$s;
				color: %3$s;
				display: table;
				text-decoration: none;
				margin: 0 auto;
				padding: 10px;
				cursor: pointer;
				-webkit-border-radius: 3px;
				-webkit-appearance: none;
				border-radius: 3px;
				white-space: nowrap;
				-webkit-box-sizing: border-box;
				-moz-box-sizing: border-box;
				box-sizing: border-box;
			}

			.button.button-large {
				padding: 18px;
			}

			.button:hover,
			.button:focus {
				background: %3$s;
				border-color: %2$s;
				color: %2$s;
			}

			.button:focus {
				border-color: %2$s;
				outline: none;
			}

			.button:active {
				background: %3$s;
				-webkit-box-shadow: inset 0 2px 5px -3px rgba(0, 0, 0, 0.5);
				box-shadow: inset 0 2px 5px -3px rgba(0, 0, 0, 0.5);
				-webkit-transform: translateY(1px);
				-ms-transform: translateY(1px);
				transform: translateY(1px);
			}',
			$styles['link'],
			$styles['link_hover'],
			$styles['content_background']
		), $styles, $this->setting );
	}

	/**
	 * Define limited WP gallery styles.
	 * @return string
	 */
	protected function get_gallery_styles() {
		return apply_filters( 'sixtenpressmaintenance_css_gallery', '
			.gallery { display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-flex: 0 1 auto; -ms-flex: 0 1 auto; flex: 0 1 auto; -webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap; overflow: hidden; }
			.gallery img { border: 1px solid #eeede8; height: auto; max-width: 100%; padding: 4px; width: auto; }
			.gallery img:hover { border: 1px solid darkgray; }
			.gallery-item { -webkit-flex: 0 0 auto; -ms-flex: 0 0 auto; flex: 0 0 auto; -webkit-flex-basis: 50%; -ms-flex-preferred-size: 50%; flex-basis: 50%; margin: 0 0 28px; max-width: 50%; text-align: center; }

			@media only screen and (min-width: 30em) {
				.gallery-columns-3 .gallery-item, .gallery-columns-6 .gallery-item,
				.gallery-columns-9 .gallery-item { -webkit-flex-basis: 33.33333%; -ms-flex-preferred-size: 33.33333%; flex-basis: 33.33333%; max-width: 33.33333%; }
				.gallery-columns-4 .gallery-item { -webkit-flex-basis: 25%; -ms-flex-preferred-size: 25%; flex-basis: 25%; max-width: 25%; }
			}

			@media only screen and (min-width: 50em) {
				.gallery-columns-5 .gallery-item { -webkit-flex-basis: 20%; -ms-flex-preferred-size: 20%; flex-basis: 20%; max-width: 20%; }
				.gallery-columns-6 .gallery-item { -webkit-flex-basis: 16.6666%; -ms-flex-preferred-size: 16.6666%; flex-basis: 16.6666%; max-width: 16.6666%; }
				.gallery-columns-7 .gallery-item { width: 14.2857%; max-width: 14.2857%; }
				.gallery-columns-8 .gallery-item { width: 12.5%; max-width: 12.5%; }
				.gallery-columns-9 .gallery-item { width: 11.1111%; max-width: 11.1111%; }
			}'
		);
	}

	/**
	 * Define image styles.
	 *
	 * @return string
	 */
	protected function get_image_styles() {
		return apply_filters( 'sixtenpressmaintenance_css_images', '
			img { max-width: 100%; height: auto; }
			.aligncenter, .alignright, .alignleft, .centered, .alignright img, .alignleft img { display: block; margin: 0 auto 24px; }
			a.alignleft, a.alignnone, a.alignright { max-width: 100%; }
			a.aligncenter img { display: block; margin: 0 auto; }
			a.alignnone { display: inline-block; }
			
			@media only screen and (min-width: 30em) {
				.alignleft { float: left; text-align: left; }
				.alignright { float: right; text-align: right; }
				img.alignnone, .alignnone { margin-bottom: 12px; }
				a.alignleft, img.alignleft, .wp-caption.alignleft { margin: 0 24px 24px 0; }
				a.alignright, img.alignright, .wp-caption.alignright { margin: 0 0 24px 24px; }
			}'
		);
	}

	/**
	 * Adjust the brightness of a CSS hex value.
	 *
	 * @param $hex
	 * @param $steps
	 *
	 * @return string
	 */
	protected function adjust_brightness( $hex, $steps ) {
		// Steps should be between -255 and 255. Negative = darker, positive = lighter
		$steps = max( - 255, min( 255, $steps ) );

		// Normalize into a six character long hex string
		$hex = str_replace( '#', '', $hex );
		if ( strlen( $hex ) === 3 ) {
			$hex = str_repeat( substr( $hex, 0, 1 ), 2 ) . str_repeat( substr( $hex, 1, 1 ), 2 ) . str_repeat( substr( $hex, 2, 1 ), 2 );
		}

		// Split into three parts: R, G and B
		$color_parts = str_split( $hex, 2 );
		$return      = '#';

		foreach ( $color_parts as $color ) {
			$color   = hexdec( $color ); // Convert to decimal
			$color   = max( 0, min( 255, $color + $steps ) ); // Adjust color
			$return .= str_pad( dechex( $color ), 2, '0', STR_PAD_LEFT ); // Make two char hex code
		}

		return $return;
	}
}
