<?php

/**
 * Add a pseudo admin bar for users who can view the site, but have no toolbar.
 *
 * Class SixTenPressMaintenanceIndicator
 */
class SixTenPressMaintenanceIndicator {

	/**
	 * SixTenPressMaintenanceIndicator constructor.
	 * @since 0.6.0
	 */
	public function __construct() {
		$setting = sixtenpressmaintenance_get_setting();
		if ( ! $setting['enabled'] ) {
			return;
		}
		add_filter( 'body_class', array( $this, 'add_body_class' ) );
		add_action( 'wp_print_footer_scripts', array( $this, 'do_style' ) );
		add_action( 'wp_footer', array( $this, 'admin_bar' ) );
	}

	/**
	 * Output the admin bar.
	 * @since 0.6.0
	 */
	public function admin_bar() {
		echo '<div id="sixtenpress-admin-bar">' . wp_kses_post( $this->get_admin_bar_content() ) . '</div>';
	}

	/**
	 * Add the default WP admin bar class to the site.
	 * @since 0.6.0
	 *
	 * @param $classes
	 *
	 * @return array
	 */
	public function add_body_class( $classes ) {
		$classes[] = 'admin-bar';

		return $classes;
	}

	/**
	 * Get the content for the admin bar.
	 * @return string
	 */
	protected function get_admin_bar_content() {
		$setting = sixtenpressmaintenance_get_setting();
		$mode    = 'maintenance' === $setting['mode'] ? __( 'Maintenance Mode', 'sixtenpress-maintenance' ) : __( 'Coming Soon Mode', 'sixtenpress-maintenance' );
		$output  = sprintf( '<div id="wp-admin-bar-sixtenpressmaintenance" class="%s"><span class="ab-icon dashicon-before dashicons-hammer"></span>%s %s</div>',
			'sixtenpressmaintenance-indicator sixtenpressmaintenance-indicator--enabled',
			$mode,
			_x( 'Enabled', 'Admin bar indicator', 'sixtenpress-maintenance' )
		);
		$output .= sprintf( '<div class="sixtenpressmaintenance-indicator"><a href="%s">%s</a></div>',
			wp_logout_url( home_url() ),
			__( 'Log Out', 'sixtenpress-maintenance' )
		);

		return $output;
	}

	/**
	 * Compile and echo the minified styles.
	 */
	public function do_style() {
		$minify = new SixTenPressMaintenanceCSSMinify();

		echo '<style type="text/css">' . esc_textarea( $minify->minify( $this->get_styles() ) ) . '</style>';
	}

	/**
	 * Return the style for the maintenance mode indicator.
	 * @return string
	 */
	protected function get_styles() {

		return 'html { margin-top: 32px !important; }
			#sixtenpress-admin-bar { direction: ltr; color: #fff; font-size: 13px; font-weight: 400; font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif; position: fixed; top: 0; width: 100%; z-index: 99999; background: #23282d; text-align: center; }
			#sixtenpress-admin-bar a { color: #eee; font-weight: 400; }
			#sixtenpress-admin-bar .sixtenpressmaintenance-indicator:hover { background-color: #32373c; }
			.sixtenpressmaintenance-indicator { display: inline-block; margin: 0 2px; padding: 6px 12px; }
			.sixtenpressmaintenance-indicator--enabled { background: rgba(159, 0, 0, 1) !important; }
			@media screen and (max-width: 782px) html { margin-top: 46px !important; } }';
	}
}
