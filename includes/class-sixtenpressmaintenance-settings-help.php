<?php

/**
 * Class SixTenPressMaintenanceSettingsHelp
 */
class SixTenPressMaintenanceSettingsHelp {

	/**
	 * Add the help tabs if 6/10 Press is not active.
	 */
	public function help() {
		$screen    = get_current_screen();
		$help_tabs = $this->define_tabs();
		if ( ! $help_tabs ) {
			return;
		}
		foreach ( $help_tabs as $tab ) {
			$screen->add_help_tab( $tab );
		}
	}

	/**
	 * Add a Maintenance help tab to Six/Ten Press.
	 * @param $tabs
	 * @param $active_tab
	 *
	 * @return array
	 */
	public function help_tab( $tabs, $active_tab ) {
		if ( 'maintenance' !== $active_tab ) {
			return $tabs;
		}

		return $this->define_tabs();
	}

	/**
	 * Define the settings page help tabs.
	 * @return array
	 */
	protected function define_tabs() {
		return array(
			array(
				'id'      => 'sixtenpress-help-maintenance-general',
				'title'   => __( 'General', 'sixtenpress-maintenance' ),
				'content' => $this->general_content(),
			),
			array(
				'id'      => 'sixtenpress-help-maintenance-simple',
				'title'   => __( 'Simple Mode', 'sixtenpress-maintenance' ),
				'content' => $this->simple_content(),
			),
			array(
				'id'      => 'sixtenpress-help-maintenance-advanced',
				'title'   => __( 'Advanced', 'sixtenpress-maintenance' ),
				'content' => $this->advanced_content(),
			),
		);
	}

	/**
	 * Content for the general maintenance help tab.
	 *
	 * @return string
	 */
	protected function general_content() {
		$content  = '<p>' . __( 'You have four options for the Maintenance Mode:', 'sixtenpress-maintenance' ) . '</p>';
		$content .= '<ol>';
		$content .= '<li>' . __( 'Disabled (all users have full access)', 'sixtenpress-maintenance' ) . '</li>';
		$content .= '<li>' . __( 'Simple Maintenance Mode: super simple message page, no scripts', 'sixtenpress-maintenance' ) . '</li>';
		$content .= '<li>' . __( 'Redirect to another URL: this cannot use a URL on this site', 'sixtenpress-maintenance' ) . '</li>';
		$content .= '<li>' . __( 'Redirect to a page on this site: best done if you can use a landing page template with no navigation, etc. Scripts, forms, etc. will function normally', 'sixtenpress-maintenance' ) . '</li>';
		$content .= '</ol>';

		return $content;
	}

	/**
	 * Content for the simple maintenance help tab.
	 *
	 * @return string
	 */
	protected function simple_content() {
		$content  = '<p>' . __( 'The Simple Maintenance Mode uses a generic <code>wp_die()</code> screen. This plugin offers minimal customization for colors and your message. Images can be added to the message, but no forms or shortcodes which require JavaScript or custom styles. This is a nice option for a simple "coming soon" or "be right back" message.', 'sixtenpress-maintenance' ) . '</p>';
		$content .= '<p>' . __( 'When using the Simple Maintenance Mode, you can tell search engines/robots whether they can crawl your site normally (Coming Soon) or if the site is unavailable (Maintenance).', 'sixtenpress-maintenance' ) . '</p>';

		return $content;
	}

	/**
	 * Content for the advanced maintenance help tab.
	 *
	 * @return string
	 */
	protected function advanced_content() {
		$content  = '<p>' . __( 'The advanced settings allow you to further customize access to your site while the Coming Soon/Maintenance Mode is active:', 'sixtenpress-maintenance' ) . '</p>';
		$content .= '<ul>';
		$content .= '<li>' . __( 'Roles: Allow only certain logged in users to access the front end of the site. Administrators always have full access.', 'sixtenpress-maintenance' ) . '</li>';
		$content .= '<li>' . __( 'Bypass URL: Allow a client/reviewer to visit the site without creating a user account for them. Give them the custom URL and they will be able to view the site normally, even while it\'s hidden otherwise.', 'sixtenpress-maintenance' ) . '</li>';
		$content .= '<li>' . __( 'Bypass by IP: IP addresses entered here will always skip the Coming Soon/Maintenance option and go directly to the site.', 'sixtenpress-maintenance' ) . '</li>';
		$content .= '</ul>';

		return $content;
	}
}
