<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width">
	<?php wp_no_robots(); ?>
	<title><?php echo esc_html( $title ) ?></title>
	<style type="text/css"><?php echo sanitize_text_field( $this->inline_style() ) ?></style>
	<?php do_action( 'sixtenpressmaintenance_head' ); ?>
</head>
<body id="maintenance-page">
<?php do_action( 'sixtenpressmaintenance_before_content' ); ?>
<div class="content">
	<?php
	do_action( 'sixtenpressmaintenance_before_message' );
	echo wp_kses_post( $message );
	do_action( 'sixtenpressmaintenance_after_message' ); ?>
</div>
<?php do_action( 'sixtenpressmaintenance_after_content' ); ?>
</body>
</html>
