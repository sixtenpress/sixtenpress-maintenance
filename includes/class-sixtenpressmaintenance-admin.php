<?php

class SixTenPressMaintenanceAdmin {

	/**
	 * @var
	 */
	protected $setting;

	/**
	 * manage capabilities
	 *
	 * @since 2.0
	 */
	public function manage_capabilities() {

		$this->setting = sixtenpressmaintenance_get_setting();

		// administrator by default
		$admin_role = get_role( 'administrator' );
		$admin_role->add_cap( 'sixtenpressmaintenance_view_site' );

		$all_roles = $this->setting['roles'];
		if ( empty( $all_roles ) ) {
			return;
		}
		$function = 'remove_cap';
		foreach ( $all_roles as $role => $value ) {
			$get_role = get_role( $role );
			if ( $value ) {
				if ( $all_roles[ $role ] ) {
					$function = 'add_cap';
				}
				$get_role->{$function}( 'sixtenpressmaintenance_view_site' );
			}
		}
	}

	/**
	 * Inject styling
	 *
	 * @since 1.1
	 */
	public function style() {
		echo '<style type="text/css">.sixtenpressmaintenance-indicator--enabled { background: rgba(159, 0, 0, 1) !important; }</style>';
	}

	/**
	 * admin bar indicator
	 *
	 * @since 1.1
	 *
	 * @param $wp_admin_bar WP_Admin_Bar
	 *
	 */
	public function indicator( $wp_admin_bar ) {
		if ( ! current_user_can( 'delete_plugins' ) ) {
			return;
		}

		if ( ! apply_filters( 'sixtenpressmaintenance_admin_bar', true ) ) {
			return;
		}

		$this->setting = sixtenpressmaintenance_get_setting();
		$is_enabled    = $this->setting['enabled'];
		$status        = _x( 'Disabled', 'Admin bar indicator', 'sixtenpress-maintenance' );
		$class         = 'sixtenpressmaintenance-indicator';
		$mode          = 'maintenance' === $this->setting['mode'] ? __( 'Maintenance Mode', 'sixtenpress-maintenance' ) : __( 'Coming Soon Mode', 'sixtenpress-maintenance' );

		if ( $is_enabled ) {
			$status = _x( 'Enabled', 'Admin bar indicator', 'sixtenpress-maintenance' );
			$class  = 'sixtenpressmaintenance-indicator sixtenpressmaintenance-indicator--enabled';
		}

		$indicator = array(
			'id'     => 'sixtenpressmaintenance',
			'title'  => '<span class="ab-icon dashicon-before dashicons-hammer"></span> ' . $mode . ' ' . $status,
			'parent' => false,
			'href'   => $this->get_settings_page_link(),
			'meta'   => array(
				'title' => _x( 'Maintenance Mode', 'Admin bar indicator', 'sixtenpress-maintenance' ),
				'class' => $class,
			),
		);
		$wp_admin_bar->add_node( $indicator );
	}

	/**
	 * Add link to plugin settings page in plugin table
	 *
	 * @param $links array
	 *
	 * @return array
	 *
	 * @since 0.5.0
	 */
	public function add_settings_link( $links ) {
		$links[] = sprintf(
			'<a href="%s">%s</a>',
			esc_url( $this->get_settings_page_link() ),
			esc_attr__( 'Settings', 'sixtenpress-featured-content' )
		);

		return $links;
	}

	/**
	 * Get the correct URL for the settings page/tab.
	 *
	 * @return mixed
	 */
	private function get_settings_page_link() {
		return add_query_arg(
			$this->get_query_args(),
			admin_url( 'options-general.php' )
		);
	}

	/**
	 * Get the correct query args for the settings page.
	 * @return array
	 */
	private function get_query_args() {
		$args = array(
			'page' => 'sixtenpressmaintenance',
		);
		if ( ! function_exists( 'sixtenpress_get_setting' ) ) {
			return $args;
		}

		return array(
			'page' => 'sixtenpress',
			'tab'  => 'maintenance',
		);
	}
}
