<?php

/**
 * main class
 *
 * @since 0.1.0
 */
class SixTenPressMaintenance {

	/**
	 * @var SixTenPressMaintenanceAdmin
	 */
	protected $admin;

	/**
	 * @var SixTenPressMaintenanceOutput
	 */
	protected $output;

	/**
	 * SixTenPressMaintenance constructor.
	 *
	 * @param $admin
	 * @param $output
	 */
	public function __construct( $admin, $output ) {
		$this->admin  = $admin;
		$this->output = $output;
	}

	/**
	 *
	 */
	public function run() {
		// Admin
		add_action( 'admin_bar_menu', array( $this->admin, 'style' ) );
		add_action( 'admin_bar_menu', array( $this->admin, 'indicator' ), 100 );
		add_action( 'admin_init', array( $this->admin, 'manage_capabilities' ) );
		add_action( 'plugins_loaded', array( $this, 'load_settings' ), 15 );
		add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );

		// Output
		add_action( 'parse_request', array( $this->output, 'init' ) );
		add_action( 'login_init', array( $this->output, 'remove_woo_password_redirect' ) );
	}

	/**
	 * Check for settings/licensing classes and create the settings page.
	 */
	public function load_settings() {
		if ( ! class_exists( 'SixTenPressSettings' ) ) {
			include_once plugin_dir_path( __FILE__ ) . '/common/class-sixtenpress-settings.php';
		}
		$files = array( 'help', 'page' );
		foreach ( $files as $file ) {
			include plugin_dir_path( __FILE__ ) . "class-sixtenpressmaintenance-settings-{$file}.php";
		}

		$settings = new SixTenPressMaintenanceSettings();
		$option   = 'sixtenpressmaintenance';
		add_action( 'admin_menu', array( $settings, 'maybe_do_settings_page' ), 20 );
		add_filter( 'sixtenpressmaintenance_get_setting', array( $settings, 'get_setting' ) );
		add_filter( "sixtenpress_sanitize_{$option}_redirect", array( $settings, 'prevent_current_url' ), 10, 4 );
		$help = new SixTenPressMaintenanceSettingsHelp();
		if ( class_exists( 'SixTenPress' ) ) {
			add_filter( 'sixtenpress_help_tabs', array( $help, 'help_tab' ), 10, 2 );
		} else {
			add_action( "load-settings_page_{$option}", array( $help, 'help' ) );
		}
		add_filter( 'plugin_action_links_' . SIXTENPRESSMAINTENANCE_BASENAME, array( $this->admin, 'add_settings_link' ) );
	}

	/**
	 * Load plugin textdomain.
	 *
	 * @since 0.1.0
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'sixtenpress-maintenance', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}
}
