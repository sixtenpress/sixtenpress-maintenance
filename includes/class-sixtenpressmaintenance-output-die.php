<?php

/**
 * Class SixTenPressMaintenanceOutputDie
 */
class SixTenPressMaintenanceOutputDie {

	/**
	 * The plugin setting.
	 * @var array
	 */
	protected $setting;

	/**
	 * @var \SixTenPressMaintenanceCSS
	 */
	protected $css;

	/**
	 * SixTenPressMaintenanceOutputDie constructor.
	 *
	 * @param $setting array
	 * @param $css
	 */
	public function __construct( $setting, $css ) {
		$this->setting = $setting;
		$this->css     = $css;
	}

	/**
	 * Simple Maintenance Mode Screen
	 *
	 * @since 1.0
	 */
	public function die() {

		do_action( 'sixtenpressmaintenance_before' );

		// remove jetpack sharing
		remove_filter( 'the_content', 'sharing_display', 19 );
		add_filter(
			'wp_die_handler',
			function () {
				return array( $this, 'replace_die' );
			}
		);
		wp_die(
			wp_kses_post( $this->get_content() ),
			esc_html( $this->setting['site_title'] ),
			array(
				'response' => (int) $this->get_mode(),
			)
		);
	}

	/**
	 * @param        $message string
	 * @param string $title
	 * @param array  $args
	 */
	public function replace_die( $message, $title = '', $args = array() ) {
		if ( ! headers_sent() ) {
			status_header( $args['response'] );
			nocache_headers();
			header( 'Content-Type: text/html; charset=utf-8' );
		}

		if ( empty( $title ) ) {
			$title = __( 'Maintenance Mode', 'sixtenpress-maintenance' );
		}
		include trailingslashit( plugin_dir_path( __FILE__ ) ) . 'views/template.php';
		die();
	}

	/**
	 * Compile and return the minified styles.
	 * @return string
	 */
	protected function inline_style() {
		$minify = new SixTenPressMaintenanceCSSMinify();

		return $minify->minify( $this->css->get_style() );
	}

	/**
	 * Get the maintenance page content.
	 * @return mixed|string
	 */
	protected function get_content() {
		return apply_filters( 'the_content', $this->setting['content'] );
	}

	/**
	 * get mode
	 *
	 * @since 0.1.0
	 */
	protected function get_mode() {
		// 200 is the maintenance page
		return 'coming_soon' === $this->setting['mode'] ? 200 : 503;
	}
}
