<?php

/**
 * Get the plugin settings.
 * @return mixed
 */
function sixtenpressmaintenance_get_setting() {
	return apply_filters( 'sixtenpressmaintenance_get_setting', false );
}

/**
 * Get the current user/visitors's IP address
 * @return string
 */
function sixtenpressmaintenance_get_current_ip() {
	$ip = '127.0.0.1';
	if ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) && strlen( $_SERVER['HTTP_X_FORWARDED_FOR'] ) > 6 ) {
		$ip = strip_tags( $_SERVER['HTTP_X_FORWARDED_FOR'] );
	} elseif ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) && strlen( $_SERVER['HTTP_CLIENT_IP'] ) > 6 ) {
		$ip = strip_tags( $_SERVER['HTTP_CLIENT_IP'] );
	} elseif ( ! empty( $_SERVER['REMOTE_ADDR'] ) && strlen( $_SERVER['REMOTE_ADDR'] ) > 6 ) {
		$ip = strip_tags( $_SERVER['REMOTE_ADDR'] );
	}
	return strip_tags( $ip );
}
