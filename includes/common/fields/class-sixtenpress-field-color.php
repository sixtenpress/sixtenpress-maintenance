<?php

class SixTenPressFieldColor extends SixTenPressFieldBase {

	/**
	 * Build a color field.
	 */
	public function do_field() {
		$default = isset( $this->field['default'] ) && $this->field['default'] ? $this->field['default'] : '';
		printf(
			'<input type="text" name="%1$s" id="%3$s" value="%2$s" class="color-field" data-default-color="%4$s">',
			esc_attr( $this->name ),
			esc_attr( $this->value ),
			esc_attr( $this->id ),
			esc_attr( $default )
		);
	}
}
