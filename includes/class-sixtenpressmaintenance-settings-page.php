<?php

/**
 * @package   SixTenPressMaintenance
 * @author    Robin Cornett
 * @link      http://robincornett.com/
 * @copyright 2015 Robin Cornett
 * @license   GPL-2.0+
 */
class SixTenPressMaintenanceSettings extends SixTenPressSettings {

	/**
	 * Plugin setting.
	 * @var array $setting
	 */
	protected $setting;

	/**
	 * The plugin settings page.
	 * @var string
	 */
	protected $page = 'sixtenpress';

	/**
	 * @var string
	 */
	protected $tab = 'sixtenpressmaintenance';

	/**
	 * The plugins settings fields.
	 * @var array
	 */
	protected $fields = array();

	/**
	 * Add a submenu page under People.
	 */
	public function maybe_do_settings_page() {

		add_action( 'admin_init', array( $this, 'register_settings' ) );

		if ( ! $this->is_sixten_active() ) {
			$this->page = $this->tab;
			add_options_page(
				__( '6/10 Press Maintenance', 'sixtenpress-maintenance' ),
				__( '6/10 Press Maintenance', 'sixtenpress-maintenance' ),
				'manage_options',
				$this->page,
				array( $this, 'do_simple_settings_form' )
			);
		}

		add_action( "load-settings_page_{$this->page}", array( $this, 'build_settings_page' ) );
		add_filter( 'sixtenpress_settings_tabs', array( $this, 'add_tab' ) );
		add_action( "sixtenpress_after_sanitize_{$this->tab}", array( $this, 'maybe_add_user' ), 10, 3 );

		$this->action  = $this->page . '_save-settings';
		$this->nonce   = $this->page . '_nonce';
		$this->setting = $this->get_setting();
		$this->fields  = $this->register_fields();

	}

	/**
	 * Build the settings page.
	 */
	public function build_settings_page() {
		add_filter( 'sixtenpress_admin_color_picker', '__return_true' );
		if ( ! $this->is_sixten_active() ) {
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_color_picker' ), 50 );
		}
		$sections = $this->register_sections();
		$this->add_sections( $sections );
		$this->add_fields( $this->fields, $sections );
	}

	/**
	 * Enqueue the color picker script.
	 */
	public function enqueue_color_picker() {
		wp_enqueue_style( 'wp-color-picker' );
		if ( function_exists( 'wp_add_inline_script' ) ) {
			wp_enqueue_script( 'wp-color-picker' );
			$code = '( function( $ ) { \'use strict\'; $( function() { $( \'.color-field\' ).wpColorPicker(); }); })( jQuery );';
			wp_add_inline_script( 'wp-color-picker', $code );
		} else {
			wp_enqueue_script( 'sixtenpress-color-picker', plugins_url( '/js/color-picker.js', __FILE__ ), array( 'wp-color-picker' ), $this->version, true );
		}
	}

	/**
	 * Add Maintenance as a tab to 6/10 Press.
	 *
	 * @param $tabs
	 *
	 * @return array
	 */
	public function add_tab( $tabs ) {
		$tabs[] = array(
			'id'  => 'maintenance',
			'tab' => __( 'Maintenance', 'sixtenpress-maintenance' ),
		);

		return $tabs;
	}

	/**
	 * Get the plugin setting, merged with defaults.
	 * @return array
	 */
	public function get_setting() {
		$setting = get_option( 'sixtenpressmaintenance', $this->defaults() );

		return wp_parse_args( $setting, $this->defaults() );
	}

	/**
	 * Set the default plugin setting.
	 * @return array
	 */
	protected function defaults() {
		$content  = sprintf( '<h1>%s</h1>', __( 'Website Under Maintenance', 'sixtenpress-maintenance' ) );
		$content .= __( 'Our website is currently undergoing scheduled maintenance. Please check back soon.', 'sixtenpress-maintenance' );

		return array(
			'enabled'    => 0,
			'content'    => $content,
			'site_title' => get_bloginfo( 'name' ) . ' | ' . __( 'Website Under Maintenance', 'sixtenpress-maintenance' ),
			'roles'      => array(),
			'mode'       => 'maintenance',
			'bypass_url' => '',
			'bypass_ip'  => '',
			'background' => '#f1f1f1',
			'text'       => '#444444',
			'link'       => '#0073aa',
			'redirect'   => '',
			'page'       => '',
			'feed'       => 0,
			'landing'    => '',
		);
	}

	/**
	 * Settings for options screen
	 *
	 * @since 1.0.0
	 */
	public function register_settings() {
		$args = array(
			'sanitize_callback' => array( $this, 'sanitize' ),
		);
		register_setting( $this->tab, $this->tab, $args );
	}

	/**
	 * Register sections for settings page.
	 *
	 * @since 0.1.0
	 */
	protected function register_sections() {
		return array(
			'main'     => array(
				'id'    => 'main',
				'title' => __( 'General Settings', 'sixtenpress-maintenance' ),
				'tab'   => 'maintenance',
			),
			'error'    => array(
				'id'    => 'error',
				'title' => __( 'Simple Maintenance Mode', 'sixtenpress-maintenance' ),
				'tab'   => 'maintenance',
			),
			'advanced' => array(
				'id'    => 'advanced',
				'title' => __( 'Advanced', 'sixtenpress-maintenance' ),
				'tab'   => 'maintenance',
			),
		);
	}

	/**
	 * Define fields for settings page.
	 * @return array
	 */
	protected function register_fields() {

		$main     = 'main';
		$advanced = 'advanced';
		$error    = 'error';

		return array(
			array(
				'id'      => 'enabled',
				'title'   => __( 'Enable Maintenance Mode', 'sixtenpress-maintenance' ),
				'type'    => 'select',
				'section' => $main,
				'choices' => array(
					0 => __( 'Disabled: Users have full access', 'sixtenpress-maintenance' ),
					1 => __( 'Simple Maintenance Mode/Error Page', 'sixtenpress-maintenance' ),
					2 => __( 'Redirect to Another URL', 'sixtenpress-maintenance' ),
					3 => __( 'Redirect to Page on This Site', 'sixtenpress-maintenance' ),
				),
			),
			array(
				'id'      => 'redirect',
				'title'   => __( 'Redirect URL', 'sixtenpress-maintenance' ),
				'type'    => 'text',
				'format'  => 'url',
				'section' => $main,
			),
			array(
				'id'       => 'page',
				'title'    => __( 'Page', 'sixtenpress-maintenance' ),
				'type'     => 'select',
				'section'  => $main,
				'choices'  => array( $this, 'get_pages' ),
				'enhanced' => true,
			),
			array(
				'id'      => 'content',
				'title'   => __( 'Content', 'sixtenpress-maintenance' ),
				'type'    => 'wysiwyg',
				'section' => $error,
				'label'   => __( 'Content for the Maintenance Mode page', 'sixtenpress-maintenance' ),
				'args'    => array(
					'textarea_name' => $this->tab . '[content]',
					'media_buttons' => true,
					'textarea_rows' => 15,
				),
			),
			array(
				'id'          => 'site_title',
				'title'       => __( 'Site Title', 'sixtenpress-maintenance' ),
				'type'        => 'text',
				'section'     => $error,
				'description' => __( 'Optionally, override the site title on the maintenance page.', 'sixtenpress-maintenance' ),
			),
			array(
				'id'          => 'mode',
				'title'       => __( 'Maintenance Mode', 'sixtenpress-maintenance' ),
				'type'        => 'select',
				'section'     => $error,
				'choices'     => array(
					'maintenance' => __( 'Maintenance Mode', 'sixtenpress-maintenance' ),
					'coming_soon' => __( 'Coming Soon Mode', 'sixtenpress-maintenance' ),
				),
				'description' => __( 'When you are logged in you\'ll see your normal website. Logged out visitors will see the Coming Soon or Maintenance page. Coming Soon Mode will be available to search engines if your site is not private. Maintenance Mode will notify search engines that the site is unavailable.', 'sixtenpress-maintenance' ),
			),
			array(
				'id'          => 'feed',
				'title'       => __( 'RSS Feed Access', 'sixtenpress-maintenance' ),
				'label'       => __( 'Optionally allow access to your RSS feed.', 'sixtenpress-maintenance' ),
				'type'        => 'checkbox',
				'section'     => $main,
				'description' => __( 'Your site must be set to Coming Soon, not Maintenance Mode (see below).', 'sixtenpress-maintenance' ),
			),
			array(
				'id'          => 'roles',
				'title'       => __( 'Roles', 'sixtenpress-maintenance' ),
				'type'        => 'checkbox_array',
				'section'     => $advanced,
				'choices'     => $this->get_roles(),
				'description' => __( 'Optionally, allow non-administrator roles to access the site. Administrators can always view the entire site.', 'sixtenpress-maintenance' ),
			),
			array(
				'id'          => 'bypass_url',
				'title'       => __( 'Bypass URL', 'sixtenpress-maintenance' ),
				'type'        => 'text',
				'section'     => $advanced,
				'description' => __( 'Allows visitors with a specific URL to bypass the maintenance/coming soon page.', 'sixtenpress-maintenance' ),
			),
			array(
				'id'          => 'bypass_ip',
				'title'       => __( 'Bypass by IP', 'sixtenpress-maintenance' ),
				'type'        => 'textarea',
				'section'     => $advanced,
				'label'       => __( 'Allows visitors coming from specific IP addresses to bypass the maintenance/coming soon page.', 'sixtenpress-maintenance' ),
				'description' => __( 'Your current IP address: ', 'sixtenpress-maintenance' ) . sanitize_text_field( sixtenpressmaintenance_get_current_ip() ),
			),
			array(
				'id'          => 'landing',
				'title'       => __( 'Bypass By Page', 'sixtenpress-maintenance' ),
				'type'        => 'textarea',
				'section'     => $advanced,
				'label'       => __( 'Allow specific pages to be accessed even with the coming soon mode enabled.', 'sixtenpress-maintenance' ),
				'description' => __( 'Allow landing pages, etc. to be accessed. Paste the slug, one per line.', 'sixtenpress-maintenance' ),
			),
			array(
				'id'      => 'background',
				'title'   => __( 'Background Color', 'sixtenpress' ),
				'section' => $error,
				'type'    => 'color',
			),
			array(
				'id'      => 'text',
				'title'   => __( 'Text Color', 'sixtenpress' ),
				'section' => $error,
				'type'    => 'color',
			),
			array(
				'id'      => 'link',
				'title'   => __( 'Link Color', 'sixtenpress' ),
				'section' => $error,
				'type'    => 'color',
			),
		);
	}

	/**
	 * @return array
	 */
	protected function get_roles() {
		$types = array();
		global $wp_roles;
		foreach ( $wp_roles->get_names() as $role => $info ) {
			$types[ $role ] = $info;
		}
		unset( $types['administrator'] );

		return $types;
	}

	/**
	 * Description for the main section.
	 * @return string
	 */
	public function main_section_description() {
		$description = __( 'Set up the kind of maintenance mode you want to have. You can use a simple page with text/images only, redirect to an external URL, or use a landing page on your site (should not include navigation or links to any other page on your site.)', 'sixtenpress-maintenance' );
		$public      = get_option( 'blog_public' );
		if ( $public ) {
			$description .= ' <strong>' . __( 'Note: your site\'s visibility is currently set to public and can be indexed by search engines.', 'sixtenpress-maintenance' ) . '</strong>';
		}

		return $description;
	}

	/**
	 * Description for the error plugin settings section.
	 */
	public function error_section_description() {
		$description  = __( 'If you choose to use a simple maintenance mode page, you can set the content and style here.', 'sixtenpress-maintenance' ) . ' ';
		$description .= sprintf( '<a href="%s" target="_blank">%s</a>', esc_url( add_query_arg( 'sixtenpressmaintenance', 'preview', home_url() ) ), esc_attr__( 'Preview the maintenance mode/coming soon page.', 'sixtenpress-maintenance' ) );

		return $description;
	}

	/**
	 * @return string
	 */
	public function advanced_section_description() {
		return __( 'These settings allow certain users to bypass your maintenance mode/coming soon page either by role, a custom URL, or their IP address.', 'sixtenpress-maintenance' );
	}

	/**
	 * Description for the bypass URL setting.
	 * @return string
	 */
	public function bypass_url_description() {
		if ( ! $this->setting['bypass_url'] ) {
			return '';
		}

		return __( 'Visitors can bypass the maintenance/coming soon page by using this URL: ', 'sixtenpress-maintenance' ) . esc_url( trailingslashit( home_url() ) . $this->setting['bypass_url'] );
	}

	/**
	 * Get the site pages to select the landing page.
	 * @return array
	 */
	protected function get_pages() {
		$options = array(
			'' => '--',
		);
		$args    = array(
			'post_type'      => 'page',
			'posts_per_page' => 100,
			'orderby'        => 'title',
			'order'          => 'ASC',
		);
		$posts   = get_posts( $args );
		foreach ( $posts as $post ) {
			$options[ $post->ID ] = get_the_title( $post );
		}

		return $options;
	}

	/**
	 * Possibly create a new user.
	 *
	 * @param $setting
	 * @param $new_value
	 * @param $old_value
	 */
	public function maybe_add_user( $setting, $new_value, $old_value ) {
		$client_view_url = isset( $new_value['bypass_url'] ) ? $new_value['bypass_url'] : false;
		$user_name       = 'sixtenpressmaintenance-' . $client_view_url;
		$this->maybe_delete_users( $user_name );
		if ( ! $client_view_url ) {
			return;
		}
		if ( username_exists( $user_name ) ) {
			return;
		}
		$user_id = wp_create_user( $user_name, wp_generate_password() );
		$user    = new WP_User( $user_id );
		$user->set_role( 'none' );
		$user->add_cap( 'sixtenpressmaintenance_view_site' );
		update_user_meta( $user_id, 'show_admin_bar_front', false );
	}

	/**
	 * If a URL on our site is set, don't accept it. Return the original value.
	 * @param $value
	 * @param $field
	 * @param $new_value
	 * @param $old_value
	 *
	 * @return mixed
	 */
	public function prevent_current_url( $value, $field, $new_value, $old_value ) {
		if ( false !== strpos( $value, home_url() ) ) {
			add_settings_error(
				$old_value,
				esc_attr( 'invalid' ),
				esc_html__( 'Sorry, you cannot set the Redirect URL to be something on this site. It has been reset to the last valid value.', 'sixtenpress-maintenance' ),
				'error'
			);
			return $old_value;
		}
		return $value;
	}

	/**
	 * Maybe delete previous bypass URL users.
	 *
	 * @param $user_name
	 */
	protected function maybe_delete_users( $user_name ) {
		$old_users = wp_get_users_with_no_role();
		if ( ! $old_users ) {
			return;
		}
		foreach ( $old_users as $user ) {
			$data = get_userdata( $user );
			if ( false !== strpos( $data->user_login, $this->tab ) && $user_name !== $data->user_login ) {
				wp_delete_user( $user );
			}
		}
	}
}
