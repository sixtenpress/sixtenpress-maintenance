<?php

/**
 * Class SixTenPressMaintenanceOutput
 */
class SixTenPressMaintenanceOutput {

	/**
	 * @var array
	 */
	protected $setting;

	/**
	 * Maybe do maintenance mode.
	 *
	 * @param $wp object
	 */
	public function init( $wp ) {
		if ( is_admin() ) {
			return;
		}
		if ( $this->is_preview_url() ) {
			$this->run_die();
		}
		if ( ! $this->can_do_maintenance() ) {
			$this->redirect_with_bypass_url();
			$this->pseudo_admin_bar();
			return;
		}
		$setting = $this->get_setting();
		if ( ! $setting['enabled'] ) {
			return;
		}
		if ( $this->coming_soon_is_feed() || $this->bypass_by_landing() ) {
			return;
		}
		if ( $this->bypass_by_url() ) {
			add_action( 'get_header', array( $this, 'login_maintenance_user' ) );
		} else {
			$this->safe_redirect( $wp );
		}
	}

	/**
	 * Redirect to the front page when using the bypass URL.
	 */
	protected function redirect_with_bypass_url() {
		$setting = $this->get_setting();
		if ( $setting['bypass_url'] && $this->get_requested_uri() === $setting['bypass_url'] ) {
			add_action( 'get_header', array( $this, 'redirect_to_home' ) );
		}
	}

	/**
	 * Add a simple admin bar if the current user doesn't have one, or isn't really logged in.
	 */
	protected function pseudo_admin_bar() {
		$meta = get_user_meta( get_current_user_id(), 'show_admin_bar_front', true );
		if ( $meta ) {
			return;
		}
		add_action( 'wp_enqueue_scripts', array( $this, 'do_indicator' ) );
	}

	/**
	 * Set up and run the safe redirect method/mode/query.
	 *
	 * @param $wp
	 */
	protected function safe_redirect( $wp ) {
		$setting = $this->get_setting();
		$url     = wp_login_url();
		switch ( $setting['enabled'] ) {
			case 1:
				$this->run_die();
				break;

			case 2:
				if ( $setting['redirect'] ) {
					$url = ( false !== strpos( $setting['redirect'], untrailingslashit( home_url() ) ) ) ? $url : untrailingslashit( $setting['redirect'] );
				}
				break;

			case 3:
				if ( $setting['page'] ) {
					unset( $wp->query_vars );
					$wp->query_vars['page_id'] = $setting['page'];
					$this->redirect_remove_actions();

					return;
				}
				break;

			default:
				$current_path = empty( $_SERVER['REQUEST_URI'] ) ? home_url() : $_SERVER['REQUEST_URI'];
				$url          = wp_login_url( $current_path );
		}

		wp_safe_redirect( $url, 302 );
		exit;
	}

	/**
	 * Load up our custom die class.
	 */
	protected function run_die() {
		$files = array( 'css', 'output-die', 'css-minify' );
		foreach ( $files as $file ) {
			include_once trailingslashit( plugin_dir_path( __FILE__ ) ) . "class-sixtenpressmaintenance-{$file}.php";
		}
		$css = new SixTenPressMaintenanceCSS();
		$die = new SixTenPressMaintenanceOutputDie( $this->get_setting(), $css );
		$die->die();
	}

	/**
	 * Remove WooCommerce lost password redirect to front end.
	 * @since 0.3.0
	 */
	public function remove_woo_password_redirect() {
		$setting = $this->get_setting();
		if ( ! $setting['enabled'] ) {
			return;
		}
		remove_filter( 'lostpassword_url', 'wc_lostpassword_url', 10 );
	}

	/**
	 * Remove standard WordPress items such as the feed links, post links, etc.
	 *
	 * @since 0.5.0
	 */
	protected function redirect_remove_actions() {
		remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
		remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
		remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
		remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
		remove_action( 'wp_head', 'index_rel_link' ); // index link
		remove_action( 'wp_head', 'parent_post_rel_link' ); // prev link
		remove_action( 'wp_head', 'start_post_rel_link' ); // start link
		remove_action( 'wp_head', 'adjacent_posts_rel_link' ); // Display relational links for the posts adjacent to the current post.
		remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
	}

	/**
	 * Should the maintenance mode page run?
	 * @return bool
	 */
	protected function can_do_maintenance() {
		$setting = $this->get_setting();
		if ( $this->bypass_by_ip() || $this->current_user_can_bypass() ) {
			// Admins, super admins can always view. Other users as granted.
			return false;
		} elseif ( ! $setting['content'] ) {
			return false;
		}

		return true;
	}

	/**
	 * Is this the preview URL?
	 *
	 * @return mixed
	 */
	protected function is_preview_url() {
		return filter_input( INPUT_GET, 'sixtenpressmaintenance', FILTER_SANITIZE_STRING );
	}

	/**
	 * Can the current user bypass the maintenance page?
	 * @return bool
	 */
	protected function current_user_can_bypass() {
		return current_user_can( 'manage_options' ) || current_user_can( 'sixtenpressmaintenance_view_site' );
	}

	/**
	 * Get the plugin setting, merged with defaults.
	 *
	 * @return mixed
	 */
	protected function get_setting() {
		if ( isset( $this->setting ) ) {
			return $this->setting;
		}

		$this->setting = sixtenpressmaintenance_get_setting();

		return $this->setting;
	}

	/**
	 * Maybe bypass the maintenance page.
	 */
	public function login_maintenance_user() {
		if ( is_user_logged_in() ) {
			return;
		}
		$setting  = $this->get_setting();
		$username = 'sixtenpressmaintenance-' . $setting['bypass_url'];
		$user     = get_user_by( 'login', $username );
		$user_id  = $user->ID;
		wp_set_current_user( $user_id, $username );
		wp_set_auth_cookie( $user_id );
		do_action( 'wp_login', $username, $user );
		$this->redirect_to_home();
	}

	/**
	 * For the maintenance mode user, add a fake admin bar.
	 *
	 * @since 0.6.0
	 */
	public function do_indicator() {
		$files = array( 'indicator', 'css-minify' );
		foreach ( $files as $file ) {
			include_once trailingslashit( plugin_dir_path( __FILE__ ) ) . "class-sixtenpressmaintenance-{$file}.php";
		}
		$indicator = new SixTenPressMaintenanceIndicator();
	}

	/**
	 * Redirect to the front page of the site.
	 *
	 * @since 0.6.0
	 */
	public function redirect_to_home() {
		wp_safe_redirect( esc_url( home_url() ) );
	}

	/**
	 * Are we using the bypass URL?
	 * @return bool
	 */
	protected function bypass_by_url() {
		$setting = $this->get_setting();

		return $setting['bypass_url'] && ( $this->get_requested_uri() === $setting['bypass_url'] );
	}

	/**
	 * See if we're on a page we've decided to allow through as a landing page.
	 *
	 * @return bool
	 */
	protected function bypass_by_landing() {
		$setting = $this->get_setting();
		$bypass  = ! empty( $setting['landing'] ) ? explode( "\r\n", $setting['landing'] ) : array();

		return (bool) in_array( $this->get_requested_uri(), $bypass, true );
	}

	/**
	 * Get the requested URI.
	 * @return string
	 */
	protected function get_requested_uri() {
		return sanitize_text_field( basename( trim( $_SERVER['REQUEST_URI'] ) ) );
	}

	/**
	 * Check whether the current IP address can bypass the maintenance page.
	 * @return bool
	 */
	protected function bypass_by_ip() {
		$setting    = $this->get_setting();
		$bypass_ip  = ! empty( $setting['bypass_ip'] ) ? explode( "\r\n", $setting['bypass_ip'] ) : array();
		$current_ip = sanitize_text_field( sixtenpressmaintenance_get_current_ip() );

		return (bool) in_array( $current_ip, $bypass_ip, true );
	}

	/**
	 * Check to see if a feed URL has been requested, and if access is allowed (requires coming soon mode and feed to be enabled).
	 * @since 0.7.0
	 *
	 * @return bool
	 */
	protected function coming_soon_is_feed() {
		$setting = $this->get_setting();

		return 'coming_soon' === $setting['mode'] && $setting['feed'] && false !== strpos( $this->get_requested_uri(), 'feed' );
	}
}
