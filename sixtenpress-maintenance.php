<?php
/**
 * Plugin Name: Six/Ten Press Maintenance
 * Plugin URI: https://gitlab.com/sixtenpress/sixtenpress-maintenance
 * Description: Very simple maintenance mode or coming soon page.
 * Version: 0.9.0
 * Author: Robin Cornett
 * URI: https://robincornett.com/
 * Text Domain: sixtenpress-maintenance
 * GitLab Plugin URI: https://gitlab.com/sixtenpress/sixtenpress-maintenance/
 * License: GPL2
 * Domain Path: /languages/
 *
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Define the plugin basename.
 */
if ( ! defined( 'SIXTENPRESSMAINTENANCE_BASENAME' ) ) {
	define( 'SIXTENPRESSMAINTENANCE_BASENAME', plugin_basename( __FILE__ ) );
}

/**
 * Load up the needed files.
 *
 * @return void
 */
function sixtenpressmaintenance_require() {
	$files = array(
		'class-sixtenpressmaintenance',
		'class-sixtenpressmaintenance-admin',
		'class-sixtenpressmaintenance-output',
		'helper-functions',
	);

	foreach ( $files as $file ) {
		require plugin_dir_path( __FILE__ ) . 'includes/' . $file . '.php';
	}
}

sixtenpressmaintenance_require();

$sixtenpressmaintenance_admin  = new SixTenPressMaintenanceAdmin();
$sixtenpressmaintenance_output = new SixTenPressMaintenanceOutput();

$sixtenpressmaintenance = new SixTenPressMaintenance(
	$sixtenpressmaintenance_admin,
	$sixtenpressmaintenance_output
);
$sixtenpressmaintenance->run();
