=== Six/Ten Press Maintenance ===
Contributors: littler.chicken
Tags: maintenance, maintenance mode, website maintenance, coming soon, under construction, offline, site maintenance,
Requires at least: 4.4
Tested up to: 5.1
Stable tag: 0.8.0
License: GPL-2.0+
License URI: http://www.gnu.org/licenses/gpl-2.0.txt

Very simple maintenance mode or coming soon page.

== Description ==

Six/Ten Press Maintenance is a very simple maintenance mode/coming soon plugin. It's mildly customizable, but still super lightweight. Filters are available for the CSS output so you can modify that as much as you like, if you're comfortable with filters.

The standard maintenance mode/coming soon page uses a modified `wp_die()` function, so it's incredibly simple. No scripts, frameworks, bells, or whistles are included.

Another coming soon page option allows you to select a page on your site to display to non authorized users. This means that all styles and scripts are available, so forms, etc. can be shown. I strongly recommend choosing a landing page for this option so that navigation and links within the site are not displayed.

This plugin also includes two features I find necessary for my sites in development:

* the ability to bypass the coming soon page by an IP address
* the ability to set a custom bypass URL to share with clients

Many thanks to <a href="https://plugins.itsluk.as/">Lukas Juhas</a> for his excellently simple Maintenance Mode plugin, which served as the jumping off point for this plugin. The new landing page option owes its existence to the similar feature in the <a href="https://10up.com/plugins/restricted-site-access-wordpress/">Restricted Access</a> plugin by the good people at 10up.

== Installation ==

1. Upload `sixtenpress-maintenance` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Navigate to Settings -> Maintenance Mode  or simply click on Admin Bar indicator for settings to enable maintenance mode.

== Frequently Asked Questions ==

= Can I change the appearance of the maintenance mode/coming soon page? =

From the plugin settings screen, you can change 1) the body background color, 2) the primary text color, and 3) the color for links/buttons. The plugin will use these as a base for its styles.

If you're comfortable with using filters, there are a handful of filters for modifying sections of the CSS:

* `sixtenpressmaintenance_css_elements`: filter to modify the array of CSS style colors and fonts
* `sixtenpressmaintenance_css_body`: filter for the general body rules
* `sixtenpressmaintenance_css_typography`: filter for typography, headings
* `sixtenpressmaintenance_css_links`: filter for general link styles/colors
* `sixtenpressmaintenance_css_buttons`: filter for links styled as buttons
* `sixtenpressmaintenance_css_gallery`: filter for galleries (only if the content includes a gallery shortcode)
* `sixtenpressmaintenance_style`: final CSS filter, also known as the One Filter

Please look through the output class to see how these are created.

= Can I add a Gravity Form (or other form) to my maintenance mode/coming soon page? =

Not to the standard maintenance mode page, but if you create a custom landing page to use as your coming soon page, then yes..

= Can I include an image gallery? =

Yes, but do not link the images. The image file URLs will be blocked, and (currently) no scripts are supported.

= The bypass URL is not working? =

The bypass URL works by creating a new user (sixtenpressmaintenance-yourcustomurl) with no privileges on the site other than bypassing the maintenance mode/coming soon page. If the URL is not working, it likely means this user was not created. Some things to check/try:

* If you're using my Six/Ten Press base plugin, please make sure you're on the latest version (at least 1.4.0).
* Try removing the bypass URL, save the settings, and then add it back.

== Screenshots ==

1. The plugin settings page
2. The default maintenance mode screen
3. The admin bar notice with the Coming Soon mode enabled

== Changelog ==

= 0.9.0 =
* added: pseudo admin bar to show to users who have "logged in" using the bypass URL but are not actually logged in

= 0.8.0 =
* added: access to specific/landing pages in settings while maintenance/coming soon mode is enabled

= 0.7.1 =
* fixed: too many redirects when no bypass URL is set

= 0.7.0 =
* added: setting to enable access to RSS feeds when the site is in coming soon mode

= 0.6.1 =
* fixed: style when indicator bar is active

= 0.6.0 =
* added: admin bar showing site status for users viewing with the bypass URL
* fixed: 404 error for repeated use of the bypass URL

= 0.5.0 =
* added: maintenance mode can now redirect to an external URL or a single page on the site

= 0.4.0 =
* added: styles for images
* changed: style functions have been extracted to a separate class

= 0.3.0 =
* added: remove the WooCommerce lost password redirect if the plugin is enabled

= 0.2.0 =
* added: limited styles for core galleries
* fixed: constant logout if bypass URL left empty

= 0.1.0 =
* initial commit/fork from original Maintenance Mode
